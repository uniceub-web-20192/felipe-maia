package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func main() {

	startServer()

	// Essa linha deve ser executada sem alteração
	// da função StartServer
}

func cebolas(w http.ResponseWriter, r *http.Request) {

	url := fmt.Sprintf("%v %v", r.Method, r.URL)
	w.Write([]byte(url))
}

func startServer() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
		Addr:        "172.22.51.143:8082",
		IdleTimeout: duration,
	}

	http.HandleFunc("/", cebolas)

	log.Print(server.ListenAndServe())
}
