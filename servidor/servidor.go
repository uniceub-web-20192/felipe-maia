package main

import (
	"io/ioutil"
	"net/http"
	"time"
)

func main() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
		Addr:        "127.0.1.1:8082",
		IdleTimeout: duration,
		//	Handler    :
	}

	http.HandleFunc("/cebola", cebolas)

	server.ListenAndServe()
}

func cebolas(w http.ResponseWriter, r *http.Request) {
	bs, err := ioutil.ReadFile("large_image9-1498775460956.jpeg")
	if err != nil {
		return
	}
	str := string(bs)
	w.Write([]byte(str))

}
